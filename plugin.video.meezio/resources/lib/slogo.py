'''
Created on 12.09.2015

@author: Kevin
'''
import xbmc
import xbmcaddon
import xbmcvfs
import os
import urllib


class Slogo:
    '''
    classdocs
    '''

    def __init__(self):
        '''
        Constructor
        '''
        Addon = xbmcaddon.Addon()
        __cwd__ = xbmc.translatePath(Addon.getAddonInfo('path')).decode("utf-8")
        self.__LogoRoot = "https://gitlab.com/neo2oo5/Meezio/raw/master/update/slogos"
        self.__noLogo = "bluewave_keinLogo.png"

        self.__icons = {}

        # OrdnerLogos
        self.__icons['FAVORITEN'] = 'separatorlogos/favoriten.png'
        self.__icons['CHANNELSEP'] = 'separatorlogos/channelsep.png'

        # SenderLogos
        self.__icons['3PLUS'] = "3+.png"
        self.__icons['3SAT'] = "3sat.png"
        self.__icons['4PLUS'] = "4+.png"
        self.__icons['ANIXE'] = "anixe sd.png"
        self.__icons['ANTENA3'] = "antena3.jpg"
        self.__icons['ARD'] = "ard-logo.png"
        self.__icons['ARD EINSFESTIVAL'] = "einsfestival.png"
        self.__icons['ARD EINSPLUS'] = "einsplus.png"
        self.__icons['ARTE'] = "arte.png"
        self.__icons['ARTEFR'] = "arte.png"
        self.__icons['ASTROTV'] = "astrotv.png"
        self.__icons['BAY3'] = None
        self.__icons['BBC WORLD'] = "bbc world.png"
        self.__icons['BIBELTV'] = "bibel.png"
        self.__icons['BLOOMBERG'] = "bloomberg.png"
        self.__icons['BR ALPHA'] = "br-alpha.png"
        self.__icons['BVN'] = "250px-BVN-logo.svg.png"
        self.__icons['CNBC'] = "cnbc.png"
        self.__icons['CNN'] = "Cnn.png"
        self.__icons['COMEDYCENTRAL'] = "comedycentral.png"
        self.__icons['CUATRO'] = "cuatro.png"
        self.__icons['DELUXEMUSIC'] = "deluxe music.png"
        self.__icons['DISNEY'] = "disney channel.png"
        self.__icons['DMAX'] = "dmax.png"
        self.__icons['DMF'] = None
        self.__icons['DWTV'] = None
        self.__icons['EURONEWS'] = "euronews.png"
        self.__icons['EUROSPORT'] = "eurosport deutschland.png"
        self.__icons['FDFTELECINCO'] = "telecinco.png"
        self.__icons['FRANKENTV'] = "franken tv.png"
        self.__icons['GOTV'] = "gotv.png"
        self.__icons['HR3'] = "hr-fernsehen.png"
        self.__icons['JOIZ'] = "joiz.png"
        self.__icons['KABEL 1'] = "kabel 1.png"
        self.__icons['KIKA'] = "kika.png"
        self.__icons['KTV'] = "k-tv.png"
        self.__icons['LA1'] = "la 1.png"
        self.__icons['LA2'] = "la 2.png"
        self.__icons['LASEXTA'] = "la sexta.png"
        self.__icons['M6'] = "m6.png"
        self.__icons['MDR'] = "mdr fernsehen.png"
        self.__icons['MUENCHENTV'] = "muenchen.tv.png"
        self.__icons['N24'] = "n24.png"
        self.__icons['NDR'] = "ndr fernsehen.png"
        self.__icons['NEOX'] = "neox.png"
        self.__icons['NICKELODEON'] = "nickelodeon.png"
        self.__icons['NOVA'] = "nova.png"
        self.__icons['NTV'] = "n-tv.png"
        self.__icons['ORF1'] = "orf1.png"
        self.__icons['ORF2'] = "orf2.png"
        self.__icons['ORF3'] = "orf iii.png"
        self.__icons['PERWY'] = None
        self.__icons['PHOENIX'] = "phoenix.png"
        self.__icons['PRO7'] = "prosieben.png"
        self.__icons['PRO7MAXX'] = "prosieben maxx.png"
        self.__icons['QLAR'] = None
        self.__icons['RAI1'] = "rai 1.png"
        self.__icons['RAI2'] = "rai 2.png"
        self.__icons['RAI3'] = "rai 3.png"
        self.__icons['RBB'] = None
        self.__icons['RHEINMAINTV'] = None
        self.__icons['RIC'] = "ric.png"
        self.__icons['RNF'] = "rnf.png"
        self.__icons['RTL'] = "rtl.png"
        self.__icons['RTL2'] = "rtl2.png"
        self.__icons['RTLNITRO'] = "rtlnitro.png"
        self.__icons['RTRPLANETA'] = "rtr planeta.png"
        self.__icons['SAT1'] = "sat 1.png"
        self.__icons['SAT1GOLD'] = "sat.1 gold.png"
        self.__icons['SF1'] = "sf 1.png"
        self.__icons['SF2'] = "sf 2.png"
        self.__icons['SIXX'] = "sixx.png"
        self.__icons['SPORT1'] = "sport 1.png"
        self.__icons['SRTL'] = "super rtl.png"
        self.__icons['STV'] = "servus tv.png"
        self.__icons['SW3'] = "swr 3.png"
        self.__icons['TAGESSCHAU24'] = "tagesschau24.png"
        self.__icons['TELE5'] = "tele 5.png"
        self.__icons['TELECINCO'] = "telecinco.png"
        self.__icons['TF1'] = "tf 1.png"
        self.__icons['TLC'] = "tlc.png"
        self.__icons['TRT'] = "trt-turk tv.png"
        self.__icons['TV5'] = "tv5.png"
        self.__icons['TVPOLONIA'] = "tvp info.png"
        self.__icons['UKALJAZEERA'] = "al jazeera channel.png"
        self.__icons['UKBBC'] = "bbc 1 london.png"
        self.__icons['UKBBC2'] = "bbc 2 england.png"
        self.__icons['UKBBC3'] = "bbc three.png"
        self.__icons['UKBBC4'] = "bbc four.png"
        self.__icons['UKCBBC'] = "cbbc channel.png"
        self.__icons['UKCBEEBIES'] = "cbeebies.png"
        self.__icons['UKCBSACTION'] = "cbs action.png"
        self.__icons['UKCBSDRAMA'] = "cbs drama.png"
        self.__icons['UKCHANNEL4'] = "channel4.png"
        self.__icons['UKCITV'] = None
        self.__icons['UKE4'] = "e4.png"
        self.__icons['UKFILM4'] = "film4.png"
        self.__icons['UKHORROR'] = "ukhorror.png"
        self.__icons['UKITV'] = "itv1.png"
        self.__icons['UKITV2'] = "itv2.png"
        self.__icons['UKITV3'] = "itv3.png"
        self.__icons['UKITV4'] = "itv4.png"
        self.__icons['UKMORE4'] = "more4.png"
        self.__icons['UKMOVIES4MEN'] = "movies4men.png"
        self.__icons['UKTRUEENTERTAINMENT'] = "true ent.png"
        self.__icons['UKTRUEMOVIES1'] = "true movies.png"
        self.__icons['UKTRUEMOVIES2'] = "true movies 2.png"
        self.__icons['USANTENNATV'] = "usAntennatv.png"
        self.__icons['USBOUNCETV'] = "bounce_ftr_logo.png"
        self.__icons['USMOVIES'] = None
        self.__icons['USNJTV'] = "njtv.png"
        self.__icons['USTHISTV'] = "This_TV_logo.svg.png"
        self.__icons['USWABC'] = "New_ABC_7_Chicago_logo.png"
        self.__icons['USWCBS'] = "uscbs.png"
        self.__icons['USWFUT'] = "WFUT_2013_Logo.png"
        self.__icons['USWNBC'] = "nbc4-logo.jpg"
        self.__icons['USWNJU'] = None
        self.__icons['USWNYE'] = "25_WNYE.svg.png"
        self.__icons['USWNYW'] = "Fox_5_WNYW_1987.svg.png"
        self.__icons['USWPIX'] = "Wpix69.jpeg"
        self.__icons['USWWOR'] = "WOR-TV_1970s.png"
        self.__icons['USWXTV'] = "130px-Univision_logo.svg.png"
        self.__icons['VIVA'] = "viva.png"
        self.__icons['VOX'] = "vox.png"
        self.__icons['W9'] = "w9.png"
        self.__icons['WDR'] = "wdr.png"
        self.__icons['WELTDERWUNDER'] = "welt der wunder.png"
        self.__icons['ZDF'] = "zdf.png"
        self.__icons['ZDF INFO'] = "zdfinfo.png"
        self.__icons['ZDF NEO'] = "zdf_neo.png"
        self.__icons['ZDFKULTUR'] = "zdf.kultur.png"

    def getIcons(self):
        return self.__icons

    def getByProgName(self, pname):
        for key, value in self.__icons.iteritems():
            if key == pname:
                if value is None:
                    return self.__LogoRoot + "/backgrounds/" + self.__noLogo
                else:
                    return self.__LogoRoot + "/" + urllib.quote(value, safe='')

        return None

    def removeUnusedLogos(self):

        dirs, files = xbmcvfs.listdir(self.__LogoRoot)

        for file in files:
            if file not in self.__icons.values():
                print file
                xbmcvfs.delete(os.path.join(self.__LogoRoot, file.decode('utf8')))
