'''
Created on 12.08.2015

@author: xoxoxo
'''
from googleapiclient.discovery import build
#from googleapiclient.errors import HttpError
#import sys
#import os
#import urllib

# Set API_KEY to the "API key" value from the "Access" tab of the
# Google APIs Console http://code.google.com/apis/console#access
# Please ensure that you have enabled the YouTube Data API and Freebase API
# for your project.
API_KEY = "AIzaSyDXp4vt9YB8Asxnx1QoeXCfWRt6mECLmjk" #Yes I did replace this with my API KEY
YOUTUBE_API_SERVICE_NAME = "youtube"
YOUTUBE_API_VERSION = "v3"


class Youtube:

    def __init__(self):
        self.youtube = build(
          YOUTUBE_API_SERVICE_NAME, 
          YOUTUBE_API_VERSION, 
          developerKey=API_KEY
        )
        
    def search(self, keyword):
        #https://www.youtube.com/watch?v=videoID
        search_response = self.youtube.search().list(
          q=keyword,
          part="id,snippet",
          maxResults=3
        ).execute()
        
        videos = []
        
        #for i in search_response.get("items", []):
        #   print i
        #  print "\n"
        
        for search_result in search_response.get("items", []):
            if search_result["id"]["kind"] == "youtube#video":

                videos.append([search_result["snippet"]["title"],
                                           search_result["id"]["videoId"],
                                           search_result["snippet"]["thumbnails"]["high"]["url"]
                                           ])
        return videos