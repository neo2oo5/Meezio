#!/usr/bin/python
# -*- coding: utf-8 -*-
import urllib
import urllib2
import urlparse

import xbmcaddon
import xbmcgui

import config
from bs4 import BeautifulSoup


class otrkeyfinder:

    availableMirrors = {
            'hq-mirror.de': 'hqmirror',
            'otr-files.de': 'otrfiles',
            'rofl-nerd.net': 'roflnerd',
            'otr-dlmirror.eu': 'otrdlmirror'
    }


    def __init__(self):
        self.__otrkeyBaseUrl = 'http://www.OtrkeyFinder.com/'
        self.__cfg = config.Config()
        self.__Addon = xbmcaddon.Addon()




    def find(self, keyword):

        plainHtml = self.__request({'search': keyword})
        soup = BeautifulSoup(plainHtml, 'html.parser')

        #print soup.prettify()
        mirror = []

        for alldivs in soup.find_all('a',  {'class': 'otrkey'}):
            res = alldivs.find_next_sibling().find_all('div',  {'class': 'mirror'})
            title = alldivs.get('href')[12:]

            print title

            for child in res:
                url = child.a.get('href')
                host = child.a.get_text()
                if host in otrkeyfinder.availableMirrors:
                    mirror.append({'TITLE': title, 'MIRROR': host, 'URL': url})

        return mirror

    def redirect(self, link):
        host = urlparse.urlparse(link).hostname
        link = str(link)
        if link.startswith('http://'):
            link = link[7:]
        if link.startswith('https://'):
            link = link[8:]
        if link.startswith('www'):
            link = link[3:]


        link = link.replace(host+'/', '')

        plainHtml = self.__request({}, link, False)

        if plainHtml is not None:
            soup = BeautifulSoup(plainHtml, 'html.parser')
            return soup.find('a',  {'id': 'mirror-link'}).get('href')

        return None

    def __request(self, payload, suburl='', Get=True):

        header = {'User-Agent': self.__cfg.get('UserAgent')}



        # The action/ target from the form
        authentication_url = self.__otrkeyBaseUrl + suburl

        # Use urllib to encode the payload
        data = urllib.urlencode(payload)

        if Get == True:
            req = urllib2.Request(authentication_url + "?" + data, None, header)
        else:
            req = urllib2.Request(authentication_url, data, header)

        print authentication_url + "?" + data
        # Make the request and read the response
        try:
            resp = urllib2.urlopen(req)
            res = resp.read()
            return res
        except urllib2.HTTPError:
            xbmcgui.Dialog().ok(self.__Addon.getAddonInfo('name'), "Es gab ein Verbindungs Problem!")
            # print e.reason

        return None

class mirror:
    def __init__(self):

        self._mirrorBaseUrl = None
        self._cfg = config.Config()
        self._Addon = xbmcaddon.Addon()


    def _request(self, payload=None,  Get=True):

        header = {'User-Agent': self._cfg.get('UserAgent')}
        if payload is None:
            payload = {}

        # The action/ target from the form
        authentication_url = self._mirrorBaseUrl

        # Use urllib to encode the payload
        data = urllib.urlencode(payload)

        if Get == True:
            req = urllib2.Request(authentication_url, None, header)
        else:
            req = urllib2.Request(authentication_url, data, header)

        print authentication_url + "?" + data
        # Make the request and read the response
        try:
            resp = urllib2.urlopen(req)
            res = resp.read()
            return res
        except urllib2.HTTPError:
            xbmcgui.Dialog().ok(self._Addon.getAddonInfo('name'), "Es gab ein Verbindungs Problem!")
            # print e.reason

        return None

class hqmirror(mirror):

    def __init__(self):
        mirror.__init__(self)


    def find(self, url):

        self._mirrorBaseUrl = url

        plainHtml = self._request()

        if plainHtml is not None:
            soup = BeautifulSoup(plainHtml, 'html.parser')

            for alldivs in soup.find_all('a',  {'class': 'download'}):
                file = alldivs.get('href')
                return file

        return None

class otrfiles(mirror):

    def __init__(self):
        mirror.__init__(self)


    def find(self, url):

        self._mirrorBaseUrl = url

        plainHtml = self._request()

        if plainHtml is not None:
            soup = BeautifulSoup(plainHtml, 'html.parser')

            for alldivs in soup.find_all('a'):
                file = alldivs.get('href')
                if '.otrkey' in file:
                    return self.find(file)

        return None

class roflnerd(mirror):

    def __init__(self):
        mirror.__init__(self)


    def find(self, url):

        self._mirrorBaseUrl = url

        plainHtml = self._request()

        linklist = []
        if plainHtml is not None:
            soup = BeautifulSoup(plainHtml, 'html.parser')

            for alldivs in soup.find_all('a'):
                file = alldivs.get('href')
                filetext = alldivs.get_text()
                print filetext
                if 'Download Server' in filetext:
                    linklist.append(file)

        if len(linklist) == 0:
            return None
        else:
            return linklist

class otrdlmirror(mirror):

    def __init__(self):
        mirror.__init__(self)


    def find(self, url):

        self._mirrorBaseUrl = url

        plainHtml = self._request()


        if plainHtml is not None:
            soup = BeautifulSoup(plainHtml, 'html.parser')

            for alldivs in soup.find_all('a', {'class': 'button'}):
                file = alldivs.get('href')
                #filetext = alldivs.get_text()
                return file


        return None

class fastmirror(mirror):

    def __init__(self):
        mirror.__init__(self)


    def find(self, url):

        self._mirrorBaseUrl = url
        plainHtml = self._request()

        self._mirrorBaseUrl = 'http://fast-mirror.de/php/functions.php'
        self._request({'postFKT': 'SetDLHash'}, False)


        if plainHtml is not None:
            soup = BeautifulSoup(plainHtml, 'html.parser')

            id= soup.find('input', {'id': 'DLBut'}).get('onclick')
            id = id[11:]
            id = id[:-1]
            print "http://fast-mirror.de/php/LimitDownloadSpeed.php?id="+id
            return "http://fast-mirror.de/php/LimitDownloadSpeed.php?id="+id


        return None
