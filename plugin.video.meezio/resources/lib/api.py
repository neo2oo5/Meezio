#!/usr/bin/python
# -*- coding: utf-8 -*-
import base64
import cookielib
import datetime
import hashlib
import os
import time
import urllib
import urllib2
import urlparse
import xml.etree.ElementTree as ET

import xbmc
import xbmcaddon
import xbmcgui

import config
from apiexception import ApiException

try:
    import StorageServer
except:
    import storageserverdummy as StorageServer

cache = StorageServer.StorageServer("myOTR/api", 1)  # (Your plugin name, Cache time in hours)


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class Api:
    __metaclass__ = Singleton

    def __init__(self):

        self.__cfg = config.Config()

        self.__Addon = xbmcaddon.Addon()
        __cwd__ = xbmc.translatePath(self.__Addon.getAddonInfo('path')).decode("utf-8")
        self.__cookie = os.path.join(__cwd__, 'myOTRCookie')

        # Store the cookies and create an opener that will hold them
        if os.path.isfile(self.__cookie) == False:
            f = open(self.__cookie, 'w')
            f.write('# Netscape HTTP Cookie File')
            f.close()

        self.__cj = cookielib.MozillaCookieJar()
        self.__cj.load(filename=self.__cookie, ignore_discard=True, ignore_expires=True)

        self.__opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(self.__cj))

        # Add our headers
        self.__opener.addheaders = [('User-agent', self.__cfg.get('UserAgent'))]

        # Install our opener (note that this changes the global opener to the one
        # we just made, but you can also just call opener.open() if you want)
        urllib2.install_opener(self.__opener)

        now = datetime.datetime.now()
        self.__today = now.strftime("%d.%m.%Y")

        self.__otrBaseUrl = "http://www.onlinetvrecorder.com"  # without ending slash
        self.__did = 158
        self.__PHPSESSID = None
        self.__checkSum = cache.cacheFunction(self.__getChecksum)
        print self.__Addon.getSetting('email')
        self.__email = self.__Addon.getSetting('email')
        self.__password = self.__Addon.getSetting('password')

    def __del__(self):
        self.__cj.save(self.__cookie, ignore_discard=True, ignore_expires=True)

    def __login(self):

        if self.__cfg.get('DevMode') == 'True':
            print "wird eingeloggt------------------------------------------------------------"

        payload = {
            'email': self.__email,
            'pass': self.__password,
            'did': self.__did,
            'checksum': self.__checkSum
        }

        contents = None

        while contents is None or not contents:
            contents = self.__request(payload, '/downloader/api/login.php', loginRequest=True)

        if "User nicht gefunden" not in contents:
            for cookie in self.__cj:
                if "PHPSESSID" in cookie.name:
                    self.__PHPSESSID = cookie.value

        else:
            xbmcgui.Dialog().ok(self.__Addon.getAddonInfo('name'), "Passwort oder Benutzername falsch!")

        self.__cj.save(self.__cookie, ignore_discard=True, ignore_expires=True)

    def getProgramList(self):
        payload = {}
        xml = self.__request(payload, '/downloader/api/stations.php')

        e = self.__parseXML(xml)

        return self.__simple_etree_to_dict(e)

    def __parseXML(self, string):
        xml = None
        trails = 0
        isRunning = True

        while isRunning and xml is None:

            try:
                xml = ET.fromstring(string)
            except ET.ParseError:
                if trails >= 3:
                    isRunning = False
                elif trails == 0:
                    self.__login()
                trails = trails + 1
                time.sleep(1)

        return xml

    def __simple_etree_to_dict(self, element, keyword='ITEM'):
        # print element.__str__()
        list = []
        try:
            for atype in element.findall(".//" + keyword):
                sublist = {}
                for el in atype.iter():
                    if keyword is not el.tag:
                        sublist[str(el.tag).upper().replace('_','')] = el.text
                        # print atype.find("TITLE").text

                list.append(sublist)
            return list
        except AttributeError:
            return list

    def __etree_to_dict(self, parent_element):

        root = {}

        if parent_element.items():
            root.update(dict(parent_element.items()))
        for element in parent_element:
            if element is not None and len(element) > 0:
                # treat like dict - we assume that if the first two tags
                # in a series are different, then they are all different.
                if len(element) == 1 or element[0].tag != element[1].tag:
                    aDict = self.__etree_to_dict(element)
                # treat like list - we assume that if the first two tags
                # in a series are the same, then the rest are the same.
                else:
                    # here, we put the list in dictionary; the key is the
                    # tag name the list elements all share in common, and
                    # the value is the list itself 
                    aDict = {element[0].tag: self.__etree_to_dict(element)}
                # if the tag has attributes, add those to the dict
                if element.items():
                    aDict.update(dict(element.items()))
                root.update({element.tag: aDict})
            # this assumes that if you've got an attribute in a tag,
            # you won't be having any text. This may or may not be a 
            # good idea -- time will tell. It works for the way we are
            # currently doing XML configuration files...
            elif element.items():
                root.update({element.tag: dict(element.items())})
            # finally, if there are no child tags and no attributes, extract
            # the text
            else:
                root.update({element.tag: element.text})

        return root

    def getEPGList(self, stationlist):
        stations = ", ".join(stationlist)

        return self.getEPG(stations)

    def getEPG(self, station, fromd=None, tod=None):

        if fromd is None:
            fromd = self.__today

        if tod is None:
            tod = self.__today

        payload = {
            'aktion': 'epg_export',
            'format': 'xml',
            'btn_ok': 'OK',
            'stations': station,
            'from': fromd,
            'to': tod
        }

        xml = self.__request(payload, '/index.php')

        e = self.__parseXML(xml)

        return self.__simple_etree_to_dict(e)

    def getProgramInfo(self, epgID):
        payload = {
            'epg_id': epgID,
            'did': self.__did,
            'checksum': self.__checkSum
        }

        xml = self.__request(payload, '/downloader/api/epg.php')

        e = self.__parseXML(xml)

        return self.__simple_etree_to_dict(e)

    def getRecordings(self):
        payload = {
            'showonly': "recordings",
            'orderby': 'time_desc',
            'did': self.__did,
            'checksum': self.__checkSum
        }

        xml = self.__request(payload, '/downloader/api/request_list2.php')

        e = self.__parseXML(xml)

        return self.__simple_etree_to_dict(e, 'FILE')

    def search(self, keyword):

        payload = {
            'aktion': "search",
            'api': 'true',
            'future': 'true',
            'searchterm': keyword,
            'did': self.__did,
            'checksum': self.__checkSum
        }

        xml = self.__request(payload, '/index.php')

        e = self.__parseXML(xml)

        return self.__simple_etree_to_dict(e, "SHOW")

    def broadcastRecord(self, epgid):
        payload = {
            'aktion': "createJob",
            'api': 'true',
            'byid': 'true',
            'email': self.__email,
            'pass': self.__password,
            'epgid': base64.b64encode(epgid)
        }

        self.__request(payload, '/index.php')

    def setFTPPushJob(self, filename):
        if self.__Addon.getSetting('ftppush') == True:
            payload = {
                'filename': filename,
                'host': self.__Addon.getSetting('ftphost'),
                'port': self.__Addon.getSetting('ftpport'),
                'username': self.__Addon.getSetting('ftpuser'),
                'password': self.__Addon.getSetting('ftppassword'),
                'directory': self.__Addon.getSetting('ftpfolder'),
                'did': self.__did,
                'checksum': self.__checkSum
            }

            return self.__simple_etree_to_dict(self.__parseXML(self.__request(payload, '/downloader/api/ftppushs.php')))
        else:
            xbmcgui.Dialog().ok(self.__Addon.getAddonInfo('name'), "FTP-Push muss in den Einstellungen erst aktiviert werden!")
            return None

    def getFTPPushJob(self):
        payload = {
            'did': self.__did,
            'checksum': self.__checkSum
        }

        return self.__simple_etree_to_dict(self.__parseXML(self.__request(payload, '/downloader/api/ftppushs.php')))

    def deleteFTPPushJob(self, pushID):
        payload = {
            'did': self.__did,
            'checksum': self.__checkSum,
            'ftppush_id': pushID
        }

        return self.__etree_to_dict(self.__parseXML(self.__request(payload, '/downloader/api/deleteftppush.php')))

    def deleteRecord(self, epgid):
        payload = {
            'aktion': "deleteJob",
            'uid': base64.b64encode(self.__email),
            'epgid': base64.b64encode(epgid)
        }

        self.__request(payload, '/index.php')

    def getDownloadInfo(self, url):
        payload = {
            'did': self.__did,
            'checksum': self.__checkSum
        }

        scheme, netloc, path, query_string, fragment = urlparse.urlsplit(url)
        query_params = urlparse.parse_qsl(query_string)

        payload.update(query_params)

        result = self.__etree_to_dict(self.__parseXML(self.__request(payload, '/downloader/api/request_file2.php')))

        return result

    def getRSS(self, **kwargs):
        print kwargs
        if 'get' in kwargs:
            return self.__simple_etree_to_dict(self.__parseXML(self.__request({}, '/rss/' + kwargs['get'] + '.php')), 'item')

        return None


    def __isSessionExpired(self):
        for cookie in self.__cj:
            return cookie.is_expired()

        return True

    def __request(self, payload, suburl, loginRequest=None):

        if self.__isSessionExpired() == True and loginRequest is None:
            self.__login()

        # The action/ target from the form
        authentication_url = self.__otrBaseUrl + suburl

        # Use urllib to encode the payload
        data = urllib.urlencode(payload)



        if self.__cfg.get('DevMode') == 'True':
            print authentication_url + "?" + data
        # Make the request and read the response
        try:
            resp = self.__opener.open(authentication_url, data, 20)
            res = resp.read()
            return res
        except urllib2.HTTPError:
            xbmcgui.Dialog().ok(self.__Addon.getAddonInfo('name'), "Es gab ein Verbindungs Problem!")
            # print e.reason
            raise ApiException("Request Timeout")

        return None

    def __getChecksum(self):
        code = self.__request({}, '/downloader/api/getcode.php', loginRequest=True)
        return hashlib.md5('f84ukgv9585hg95g' + code + 's392cft49ugh54').hexdigest()
