#!/usr/bin/python
# -*- coding: utf-8 -*-


import ConfigParser, os
import xbmc, xbmcaddon



class Singleton(type):
    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]



class Config:
    __metaclass__ = Singleton

    def __init__(self):

        Addon = xbmcaddon.Addon()
        __cwd__ = xbmc.translatePath(Addon.getAddonInfo('path')).decode("utf-8")
        self.__CONFIG = os.path.join(__cwd__, 'resources', 'data', 'config.cfg')

        self.__defaultSection = "DEFAULT"
        self.__config = ConfigParser.ConfigParser()
        self.__config.read(self.__CONFIG)

    """
    cfg.set(**{'tk1': 'vl1', 'tk2': 'vl2'})
    cfg.set(lk1='vl1', lk2='vl2')
    """
    def set(self, **kwargs):

        for key, value in kwargs.iteritems():
            self.__config.set(self.__defaultSection, key, value)

        with open(self.__CONFIG, 'wb') as configfile:
            self.__config.write(configfile)

    def get(self, key):
        return self.__config.get(self.__defaultSection, key)