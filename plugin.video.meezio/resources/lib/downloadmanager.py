#!/usr/bin/python
# -*- coding: utf-8 -*-
import urllib
import urllib2

import xbmcaddon
import xbmcgui

import config


class jdownloader2:

    def __init__(self, durl):
        self.__Addon = xbmcaddon.Addon()
        self.__cfg = config.Config()

        url = 'http://' + self.__Addon.getSetting('jd2ip') + ':' + self.__cfg.get('JD2Port') + '/flash/add'
        datad = {'urls': durl}

        header = {'User-Agent': self.__cfg.get('UserAgent')}


        data = urllib.urlencode(datad)
        req = urllib2.Request(url, data, header)
        try:
            self.response = urllib2.urlopen(req)
        except urllib2.URLError:
            xbmcgui.Dialog().ok(self.__Addon.getAddonInfo('name'), "JDownloader2 ist nicht erreichbar. Überprüfe die IP und achte darauf das JD2 läuft!")