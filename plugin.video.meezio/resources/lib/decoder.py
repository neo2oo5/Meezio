#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import platform
import subprocess
import tarfile
import tempfile
import threading
import urllib

import xbmcaddon
import xbmcgui
import xbmcvfs

import filesystem


class easydecoder:


    def __init__(self):
        self.__Addon = xbmcaddon.Addon()
        self.__currentOS = platform.system()
        self.__currentOSArch = platform.architecture()[0]
        self.__machine = platform.machine()

        self.__osConfig = {
            'Windows': {'dlink': "http://www.onlinetvrecorder.com/downloads/otrdecoder-bin-i586-pc-mingw32msvc-0.4.1132.tar.bz2",
                        'bin': 'otrdecoder.exe'
                    },
            'Linux': {

                        {'default':
                             {  'dlink': "http://www.onlinetvrecorder.com/downloads/otrdecoder-bin-linux-Ubuntu_8.04-i686-0.4.1132.tar.bz2",
                                'bin': 'otrdecoder'
                             }
                        }
                      },
            'Darwin': {'dlink': "http://www.onlinetvrecorder.com/downloads/otrdecoder-bin-universal-apple-darwin9-0.4.1132.tar.bz2",
                       'bin': 'otrdecoder'
                    }
        }

        self.__file = self.__osConfig[self.__currentOS]['dlink'].split("/")
        self.__file = self.__file[len(self.__file)-1]
        self.__folder = self.__file[:-8]


    def __download(self):

        decoder = urllib.URLopener()
        decoder.retrieve(self.__osConfig[self.__currentOS]['dlink'], os.path.join(tempfile.gettempdir(), self.__file))

    def __extract(self):
        tar = tarfile.open(os.path.join(tempfile.gettempdir(), self.__file))
        tar.extractall(filesystem.filesystem().getDecoderFolder())
        tar.close()

    def install(self):
        if xbmcvfs.exists(os.path.join(tempfile.gettempdir(), self.__file)) == 0:
            self.__download()

        if xbmcvfs.exists(os.path.join(filesystem.filesystem().getDecoderFolder(), self.__folder, self.__osConfig[self.__currentOS]['bin'])) == 0:
            self.__extract()

    def decode(self, otrkey):
        if xbmcvfs.exists(os.path.join(filesystem.filesystem().getDecoderFolder(), self.__folder, self.__osConfig[self.__currentOS]['bin'])) == 1:
            if xbmcvfs.exists(otrkey) == 1:
                decode = _decode(otrkey, self.__folder, self.__osConfig, self.__currentOS)
                decode.start()

                self.__decodeList.append(decode)
                xbmcgui.Dialog().ok(self.__Addon.getAddonInfo('name'), "Sendung wird decodiert.")
            else:
                xbmcgui.Dialog().ok(self.__Addon.getAddonInfo('name'), "Sendung konnte nicht decodiert werden!")
        else:
            result = xbmcgui.Dialog().yesno(heading="Easydecoder",
                                                line1="Easydecoder ist noch nicht installiert\n Soll er jetzt installiert werden?", yeslabel="JA",
                                                nolabel="NEIN")

            if result == 1:
                self.install()
                xbmcgui.Dialog().ok(self.__Addon.getAddonInfo('name'), "Easydecoder wurde installiert. Bitte wiederholen sie die Decodierung")


class _decode(threading.Thread):
    def __init__(self, otrkey, folder, osconfig, cos):

        threading.Thread.__init__(self)


        self.__Addon = xbmcaddon.Addon()
        self.stdout = None
        self.stderr = None
        self.__otrkey = otrkey
        self.__folder = folder
        self.__osConfig = osconfig
        self.__currentOS = cos



    def run(self):

        flags = ' -e ' + self.__Addon.getSetting('email') + ' -p ' + self.__Addon.getSetting('password') + ' -o ' + os.path.join(
            filesystem.filesystem().getDecoderFolder(), 'decoded')
        flags += ' -i "' + self.__otrkey +'"'' -C auto'
        cmd = os.path.join(filesystem.filesystem().getDecoderFolder(), self.__folder, self.__osConfig[self.__currentOS]['bin']) + flags

        self.__p = subprocess.Popen(cmd,
                             shell=True,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)

        self.stdout, self.stderr = self.__p.communicate()

        self.__p.wait()

        xbmcvfs.rename(self.__otrkey, self.__otrkey+".old")

    def getOTRKey(self):
        otrkey = self.__otrkey.split(os.sep)
        return otrkey[len(otrkey)-1]
