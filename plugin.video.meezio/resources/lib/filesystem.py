import xbmcaddon
import xbmcvfs
import os


class filesystem:

    def __init__(self):
        self._Addon = xbmcaddon.Addon()
        self._otrPath = self._Addon.getSetting('download')
        self._easydecoderPath = self._Addon.getSetting('easydecoder')

        self._VideoFormats = [
            'avi', 'mpg', 'mp4'
        ]




    def findOTRKeyFiles(self, Folder=None):

        if Folder is None:
            Folder = self._otrPath

        dirs, files = xbmcvfs.listdir(Folder)

        otrkeyList = []

        for onedir in dirs:
            tmplist = self.findOTRKeyFiles(os.path.join(Folder, onedir))
            otrkeyList.extend(tmplist)


        for file in files:
            filetmp = file.split('.')
            if 'otrkey' in filetmp[len(filetmp)-1]:
                otrkeyList.append(os.path.join(Folder, file))

        return otrkeyList

    def findALLVideos(self, Folder=None):

        if Folder is None:
            Folder = self._otrPath

        dirs, files = xbmcvfs.listdir(Folder)

        otrkeyList = []

        for onedir in dirs:
            tmplist = self.findALLVideos(os.path.join(Folder, onedir))
            otrkeyList.extend(tmplist)


        for file in files:
            filetmp = file.split('.')

            for vformat in self._VideoFormats:
                if vformat in filetmp[len(filetmp)-1]:
                    otrkeyList.append(os.path.join(Folder, file))

        return otrkeyList

    def getDecoderFolder(self):
        if "None" in self._easydecoderPath :
            return self._otrPath
        else:
            return self._easydecoderPath
