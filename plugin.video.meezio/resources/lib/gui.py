#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import textwrap
import threading
import traceback

import resources.lib.api as api
import resources.lib.yt as yt
import xbmc
import xbmcaddon
import xbmcgui

import config
import decoder
import downloadmanager
import filesystem
import mirror
import re

try:
    import StorageServer
except:
    import storageserverdummy as StorageServer

cache = StorageServer.StorageServer("myOTR/gui", 24)


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class windowManager:
    __metaclass__ = Singleton

    # static
    programView = 1
    downloadView = 2
    recordingView = 3
    ftppushView = 4
    searchView = 5
    mediaView = 6
    infoView = 7

    def __init__(self, **kwargs):

        self.__lastkwargs = None
        self._kwargs = kwargs
        self._changeWindow = True
        self._windowHistory = []
        self._windowbackCount = 1
        self._nextWindow = 1  # default
        self._isClosed = False
        self._mywindow = None

    def show(self):
        t1 = threading.Thread(target=self.__show())
        t1.start()

        print "ende"


    def __show(self):

        while self._isClosed == False:

            if self._changeWindow is True:
                if self._nextWindow == 1:
                    #try:
                        self._mywindow = programView()
                    #except Exception:
                       # break
                elif self._nextWindow == 2:
                    try:
                        self._mywindow = downloadView()
                    except Exception:
                        self.setLastWindow()
                elif self._nextWindow == 3:
                    try:
                        self._mywindow = recordingView()
                    except Exception:
                        self.setLastWindow()

                elif self._nextWindow == 4:
                    try:
                        self._mywindow = ftppushView()
                    except Exception:
                        self.setLastWindow()

                elif self._nextWindow == 5:
                    try:
                        self._mywindow = searchView()
                    except Exception:
                        self.setLastWindow()

                elif self._nextWindow == 6:
                    try:
                        self._mywindow = mediaView()
                    except Exception:
                        self.setLastWindow()
                elif self._nextWindow == 7:
                    try:
                        self._mywindow = infoView()
                    except Exception:
                        self.setLastWindow()

                traceback.print_exc()




            if self._mywindow is not None:





                if self._mywindow.isabortRequested():
                    self._mywindow.setClosed(True)
                    self._mywindow.close()
                    break
                else:
                    if self._mywindow.isPaused() == False:

                        if xbmc.Player().isPlayingVideo() == False and self._mywindow.isClosed() == False:
                            sleeptime = 1
                            self._mywindow.show()
                            self._changeWindow = False
                            self._isClosed = self._mywindow.isClosed()
                        else:
                            sleeptime = 5
                            self._mywindow.setPaused(True)
                            self._mywindow.close()
                            xbmc.executebuiltin("Dialog.Close(busydialog)")

                    else:
                        #reset pause
                        if xbmc.Player().isPlayingVideo() == False:
                            sleeptime = 1
                            self._mywindow.setPaused(False)



                    if xbmc.Monitor().waitForAbort(sleeptime):
                        return






        del self._mywindow


    def setWindow(self, windowID=1, **kwargs):
        self._changeWindow = True
        self._nextWindow = windowID
        self._kwargs = kwargs

        for item in self._windowHistory:
            if item['windowID'] == windowID:
                self._windowHistory.remove(item)

        self._windowHistory.insert(0, {'windowID': windowID, 'kwargs': kwargs})

    def setLastWindow(self):
        if len(self._windowHistory) > self._windowbackCount:
            self._nextWindow = self._windowHistory[self._windowbackCount]['windowID']
            self._kwargs = self._windowHistory[self._windowbackCount]['kwargs']
            self._changeWindow = True
            self._windowbackCount = self._windowbackCount + 1

    def getLastWindow(self):
        if len(self._windowHistory) > self._windowbackCount:
            return self._windowHistory[self._windowbackCount]['windowID']
        else:
            return None

    def getKwargs(self):
        return self._kwargs


class OTRView(xbmcgui.WindowDialog):
    def __init__(self, pagination=False):

        # You need to call base class' constructor.
        # xbmcgui.Window.__init__(self, self._cfg.get('AddonName'))

        self._cfg = config.Config()
        self._api = api.Api()
        self._Addon = xbmcaddon.Addon()
        self._isClosed = False
        self._isPaused = False
        self._row = 5
        self._column = 5
        self._lastRow = 0
        self._lastColumn = 0
        self._marginTop = 50
        self._marginLeft = 10
        self._cButtonID = None
        self._bButtonID = None

        self._width = 1280
        self._height = 700
        self._windowManager = windowManager()
        self._kwargs = self._windowManager.getKwargs()
        self._pagination = pagination

        #Add BGImage
        __cwd__ = xbmc.translatePath(self._Addon.getAddonInfo('path')).decode("utf-8")
        self.main_bg_img = os.path.join(__cwd__, 'resources', 'media', 'v2.png')


        self.main_bg = xbmcgui.ControlImage(1, 1, 1280, 720, self.main_bg_img)
        self.addControl(self.main_bg)

        self._paginationFocusList = []

        if self._pagination == True:
            if 'page' not in self._kwargs:
                self._kwargs['page'] = 1

            self._bPrevious = xbmcgui.ControlButton(x=self._marginLeft + 50, y=self._marginTop + 600, width=130, height=40, label=u'Seite zurück')
            self.addControl(self._bPrevious)
            self._paginationFocusList.append(self._bPrevious.getId())

            self._bNext = xbmcgui.ControlButton(x=self._marginLeft + 185, y=self._marginTop + 600, width=130, height=40, label=u'Seite vor')
            self.addControl(self._bNext)
            self._paginationFocusList.append(self._bNext.getId())


        if self._windowManager.getLastWindow() is not None:
            self._bButtonID = xbmcgui.ControlButton(920, 630, 150, 50, 'Zurück')
            self.addControl(self._bButtonID)
            self._paginationFocusList.append(self._bButtonID.getId())

        self._cButtonID = xbmcgui.ControlButton(1090, 630, 150, 50, 'Beenden')
        self.addControl(self._cButtonID)
        self._paginationFocusList.append(self._cButtonID.getId())

    def onControl(self, control):
        cid = control.getId()

        if cid == self._cButtonID.getId():
            self._isClosed = True
            self.close()

        if self._bButtonID is not None:
            if cid == self._bButtonID.getId():
                self._windowManager.setLastWindow()

    def onAction(self, action):

        if action == xbmcgui.ACTION_NAV_BACK or action == xbmcgui.ACTION_PREVIOUS_MENU:
            self._isClosed = True
            self.close()

    def isClosed(self):
        return self._isClosed

    def setClosed(self, state):
        self._isClosed = state

    def isPaused(self):
        return self._isPaused

    def setPaused(self, state):
        self._isPaused = state

    def isabortRequested(self):
        return xbmc.abortRequested


class programView(OTRView):
    def __init__(self):

        OTRView.__init__(self)

        if "epgID" in self._kwargs:
            epgData = cache.cacheFunction(self._api.getProgramInfo, self._kwargs['epgID'])

        epgData = epgData[0]



        yout = yt.Youtube()
        ytres = yout.search(epgData["TITLE"] + " HD Trailer German")

        '''
        Column Left
        '''
        # row1
        self.addControl(xbmcgui.ControlLabel(x=self._marginLeft + 50, y=self._marginTop + 50, width=500, height=25,
                                             label=u'Begin: ' + epgData["NICEBEGIN"]))
        self.addControl(xbmcgui.ControlLabel(x=self._marginLeft + 300, y=self._marginTop + 50, width=500, height=25,
                                             label=u'End: ' + epgData["NICEEND"]))
        # row2
        self.addControl(xbmcgui.ControlLabel(x=self._marginLeft + 50, y=self._marginTop + 75, width=500, height=25,
                                             label=u'Genre: ' + epgData["TYP"]))
        self.addControl(xbmcgui.ControlLabel(x=self._marginLeft + 300, y=self._marginTop + 75, width=500, height=25,
                                             label=u'Dauer: ' + epgData["DURATION"]))

        if epgData["FSK"] is not None:
            self.addControl(xbmcgui.ControlLabel(x=350, y=75, width=500, height=25, label=u'FSK: ' + epgData["FSK"]))

        # row3
        self.addControl(xbmcgui.ControlLabel(x=self._marginLeft + 50, y=self._marginTop + 100, width=500, height=25,
                                             label=u'Titel: ' + epgData["TITLE"]))

        # row4
        self.addControl(xbmcgui.ControlLabel(x=self._marginLeft + 50, y=self._marginTop + 150, width=700, height=100,
                                             label=u'Beschreibung: \n' + textwrap.fill(epgData["TEXT"], 70)))

        '''
        Column Rights
        '''
        #TrailerImage
        self.addControl(xbmcgui.ControlImage(750, 75, 500, 400, ytres[0][2]))

        self.__ytButtons = []
        self.__sButtons = []

        if len(ytres) >= 1 and len(ytres[0]) >= 2:
            ytb = xbmcgui.ControlButton(750, 500, 150, 50, 'Trailer 1')
            self.addControl(ytb)
            self.__ytButtons.append({'video': ytres[0][1], 'keyID': ytb.getId()})
        if len(ytres) >= 2 and len(ytres[1]) >= 2:
            ytb = xbmcgui.ControlButton(920, 500, 150, 50, 'Trailer 2')
            self.addControl(ytb)
            self.__ytButtons.append({'video': ytres[1][1], 'keyID': ytb.getId()})
        if len(ytres) >= 3 and len(ytres[2]) >= 2:
            ytb = xbmcgui.ControlButton(1090, 500, 150, 50, 'Trailer 3')
            self.addControl(ytb)
            self.__ytButtons.append({'video': ytres[2][1], 'keyID': ytb.getId()})

        #ServiceButtons
        sb = xbmcgui.ControlButton(750, 560, 150, 50, 'Download')
        self.addControl(sb)
        self.__sButtons.append({'keyID': sb.getId(), 'service': self._windowManager.downloadView})

        sb = xbmcgui.ControlButton(920, 560, 150, 50, 'Aufnehmen')
        self.addControl(sb)
        self.__sButtons.append({'keyID': sb.getId(), 'service': self._windowManager.recordingView})

        sb = xbmcgui.ControlButton(1090, 560, 150, 50, 'Ansehen')
        self.addControl(sb)
        self.__sButtons.append({'keyID': sb.getId(), 'service': None})

        self.__FocusButtons = [self.__ytButtons, self.__sButtons]


    def onControl(self, control):

        OTRView.onControl(self, control)



        video = None
        for button in self.__ytButtons:
            if control.getId() == button['keyID']:
                video = button['video']

        for button in self.__sButtons:
            if "epgID" in self._kwargs:
                if button['service'] == self._windowManager.downloadView and control.getId() == button['keyID']:
                    self._windowManager.setWindow(self._windowManager.downloadView, **{'epgID': self._kwargs['epgID']})
                if button['service'] == self._windowManager.recordingView and control.getId() == button['keyID']:

                    self._api.broadcastRecord(self._kwargs['epgID'])

                    result = xbmcgui.Dialog().yesno(heading="Aufnahmeliste",
                                                line1="Aufnahme Programmiert. \nMöchten Sie zur Aufnahmeliste?", yeslabel="JA",
                                                nolabel="NEIN")

                    if result == 1:
                        self._windowManager.setWindow(self._windowManager.recordingView)

                if  button['service'] == None and control.getId() == button['keyID']:

                    # aufnahme prog.
                    self._api.broadcastRecord(self._kwargs['epgID'])

                    listR = self._api.getRecordings()

                    epgData = None
                    for item in listR:
                        if self._kwargs['epgID'] == item['EPGID']:
                            epgData = item

                    listD = self._api.getDownloadInfo(epgData['FILEREQUEST'])

                    print listD
                    if "MP4_Stream" in listD:
                        xbmc.Player().play(listD["MP4_Stream"]["FREE"])



        if video is not None:
            playback_url = 'plugin://plugin.video.youtube/play/?video_id=%s' % (video)

            if playback_url:
                xbmc.Player().play(playback_url)
            else:
                print "Fehler beim abspielen vom Trailer"


    def onAction(self, action):


        #todo_neuschreiben


        try:
            self.__focusColumn
        except AttributeError:
            self.__focusColumn = 0
            self.__focusRow = 0
            self.__focuspRow = 0
            self.__focusPagination = False


        if action.getId() == xbmcgui.ACTION_MOVE_RIGHT:
            if self.__focusRow < (len(self.__FocusButtons[self.__focusColumn]) - 1):
                self.__focusRow = self.__focusRow + 1
            else:
                 if self.__focuspRow < (len(self._paginationFocusList)-1):
                    self.__focuspRow = self.__focuspRow + 1



        if action.getId() == xbmcgui.ACTION_MOVE_LEFT:
            if self.__focusRow > 0 and self.__focusPagination == False:
                self.__focusRow = self.__focusRow - 1
            else:
                if self.__focuspRow > 0:
                    self.__focuspRow = self.__focuspRow -1

        if action.getId() == xbmcgui.ACTION_MOVE_UP:
            if self.__focusColumn > 0 and self.__focusPagination == False:
                self.__focusColumn = self.__focusColumn - 1
            else:
                self.__focusPagination = False




        if action.getId() == xbmcgui.ACTION_MOVE_DOWN:
            if self.__focusColumn < (len(self.__FocusButtons) - 1) and self.__focusPagination == False:
                self.__focusColumn = self.__focusColumn + 1
            else:
                self.__focusPagination = True



        if self.__focusPagination == False:
            self.setFocusId(self.__FocusButtons[self.__focusColumn][self.__focusRow]['keyID'])
        else:
            self.setFocusId(self._paginationFocusList[self.__focuspRow])


class downloadView(OTRView):
    def __init__(self):

        OTRView.__init__(self, True)
        epgID = None

        if "epgID" in self._kwargs:
            epgID = self._kwargs["epgID"]

        if epgID is None:
            xbmcgui.Dialog().ok(self._Addon.getAddonInfo('name'), "Keine EPGID gefunden!")
            raise Exception

        # aufnahme prog.
        self._api.broadcastRecord(epgID)

        listR = self._api.getRecordings()

        epgData = None
        for item in listR:
            if epgID == item['EPGID']:
                epgData = item

        if epgData is None:
            xbmcgui.Dialog().ok(self._Addon.getAddonInfo('name'), "Sendung steht zum Download noch nicht bereit!")
            raise Exception




        if 'downloadData' not in self._kwargs:


            t1 = threading.Thread(target=xbmcgui.Dialog().yesno, kwargs={'heading': "Dialog schließt in %s sek" % 3,
                                                                     'line1': "Download-Links werden geladen",
                                                                     'yeslabel': "ok", 'nolabel': "abbrechen",
                                                                     'autoclose': int(3 * 1000)})
            t1.start()

            listD = self._api.getDownloadInfo(epgData['FILEREQUEST'])
            self._kwargs['downloadData'] = listD


        else:
            listD = self._kwargs['downloadData']



        self.__buttonC = []
        self.__buttonP = []

        if 'mirrorList' not in self._kwargs:
            self._kwargs['mirrorList'] = []

        self.__currentpageRowCount = 1
        self.__maxpageRowCount = 4
        self.__max = (self.__maxpageRowCount * self._kwargs['page'])
        self.__min = (self.__maxpageRowCount * (self._kwargs['page'] - 1))

        otrkeylist = []

        for item in listD:
            if self.__currentpageRowCount <= self.__max and self.__currentpageRowCount > self.__min :
                self.addControl(xbmcgui.ControlLabel(x=self._marginLeft + 50, y=self._marginTop + 50, width=1000, height=50,
                                                     label=u'Titel: ' + listD[item]['FILENAME']))
                self.addControl(xbmcgui.ControlLabel(x=self._marginLeft + 50, y=self._marginTop + 75, width=200, height=50,
                                                     label=u'Größe: ' + listD[item]['SIZE']))

                self.addControl(xbmcgui.ControlLabel(x=self._marginLeft + 255, y=self._marginTop + 75, width=200, height=50,
                                                     label=u'Type: Direct'))

                self.addControl(xbmcgui.ControlLabel(x=self._marginLeft + 50, y=self._marginTop + 100, width=100, height=50,
                                                     label=u'Kosten: '))
                self.addControl(
                    xbmcgui.ControlLabel(x=self._marginLeft + 155, y=self._marginTop + 100, width=150, height=50,
                                         label=u'FREE: ' + listD[item]['GWPCOSTS']['FREE'] + ' Cent'))
                button = xbmcgui.ControlButton(x=self._marginLeft + 310, y=self._marginTop + 100, width=100, height=30,
                                               label=u'JD2')
                self.addControl(button)
                self.__buttonC.append({button.getId(): listD[item]['FREE'], 'keyID':button.getId()})

                #ftppush
                self.addControl(
                    xbmcgui.ControlLabel(x=self._marginLeft + 415, y=self._marginTop + 100, width=150, height=50,
                                         label=u'FREE: ' + listD[item]['GWPCOSTS']['FREE'] + ' Cent'))
                button = xbmcgui.ControlButton(x=self._marginLeft + 570, y=self._marginTop + 100, width=150, height=30,
                                               label=u'FTP-Push')
                self.addControl(button)
                self.__buttonP.append({button.getId(): listD[item]['FREE'], 'filename': listD[item]['FILENAME'], 'keyID':button.getId()})

                self.addControl(
                    xbmcgui.ControlLabel(x=self._marginLeft + 155, y=self._marginTop + 135, width=150, height=50,
                                         label=u'PRIO: ' + listD[item]['GWPCOSTS']['PRIO'] + ' Cent'))
                button = xbmcgui.ControlButton(x=self._marginLeft + 310, y=self._marginTop + 135, width=100, height=30,
                                               label=u'JD2')
                self.addControl(button)
                self.__buttonC.append({button.getId(): listD[item]['PRIO'], 'keyID':button.getId()})

                self._marginTop = self._marginTop + 140


                filetmp = listD[item]['FILENAME'].split('.')
                if 'otrkey' in filetmp[len(filetmp)-1]:
                    otrkeylist.append(listD[item]['FILENAME'])
            self.__currentpageRowCount = self.__currentpageRowCount + 1



        mirrorData = mirror.otrkeyfinder()

        for key in otrkeylist:
            self._kwargs['mirrorList'].extend(mirrorData.find(key))





        if len(self._kwargs['mirrorList']) > 0:
            for mitem in self._kwargs['mirrorList']:

                if self.__currentpageRowCount <= self.__max and self.__currentpageRowCount > self.__min :
                    self.addControl(xbmcgui.ControlLabel(x=self._marginLeft + 50, y=self._marginTop + 50, width=1000, height=50,
                                                         label=u'Titel: ' + mitem['TITLE']))
                    self.addControl(xbmcgui.ControlLabel(x=self._marginLeft + 50, y=self._marginTop + 75, width=200, height=50,
                                                         label=u'Größe: Unbekannt'))

                    self.addControl(xbmcgui.ControlLabel(x=self._marginLeft + 255, y=self._marginTop + 75, width=200, height=50,
                                                         label=u'Type: Mirror'))

                    self.addControl(xbmcgui.ControlLabel(x=self._marginLeft + 460, y=self._marginTop + 75, width=200, height=50,
                                                         label=u'Hoster: ' + mitem['MIRROR']))


                    self.addControl(
                        xbmcgui.ControlLabel(x=self._marginLeft + 155, y=self._marginTop + 100, width=150, height=50,
                                             label=u'FREE: 0 Cent'))
                    button = xbmcgui.ControlButton(x=self._marginLeft + 310, y=self._marginTop + 100, width=100, height=30,
                                                   label=u'JD2')
                    self.addControl(button)
                    self.__buttonC.append({'%sMIRROR' % str(button.getId()): mitem, 'keyID':button.getId()})

                    self._marginTop = self._marginTop + 140
                self.__currentpageRowCount = self.__currentpageRowCount + 1

        self.__buttonF= [self.__buttonC, self.__buttonP]



    def onControl(self, control):

        OTRView.onControl(self, control)

        cid = control.getId()

        #ftppush
        for item in self.__buttonP:
            if item['keyID'] == cid:
                result = self._api.setFTPPushJob(item['filename'])

                if result is not None:
                    if 'ok' in result['RESULT']:
                        xbmcgui.Dialog().ok(self._Addon.getAddonInfo('name'), "FTP-Push Auftrag wurde erstellt.")
                    else:
                        xbmcgui.Dialog().ok(self._Addon.getAddonInfo('name'), "Beim erstellen des FTP-Push Auftrags kam es zu einem Problem")


        #directDownloads
        for item in self.__buttonC:
            if item['keyID'] == cid:
                downloadmanager.jdownloader2(item[cid])

        #mirrorDownload
        for item in self.__buttonC:
            if item['keyID'] == cid:
                mirrorhost = self.__buttonC[str(cid)+'MIRROR']['MIRROR']

                if mirrorhost in mirror.otrkeyfinder.availableMirrors:
                    otrkey = mirror.otrkeyfinder()

                    module = __import__('mirror')
                    class_ = getattr(module, mirror.otrkeyfinder.availableMirrors[mirrorhost])
                    host = class_()
                    link = host.find(otrkey.redirect(self.__buttonC[str(cid)+'MIRROR']['URL']))

                    if link is not None:
                        if isinstance(link, basestring):
                            jd2 = downloadmanager.jdownloader2(link)
                            del jd2
                        else:
                            link = '\r\n'.join(link)
                            jd2 = downloadmanager.jdownloader2(link)
                            del jd2
                    else:
                        xbmcgui.Dialog().ok(self._Addon.getAddonInfo('name'), "Bei diesem Mirror gibt es ein Problem!")


        if cid == self._bNext.getId():
            if self.__currentpageRowCount >= self.__max:
                self._kwargs['page'] = self._kwargs['page'] + 1
                self._windowManager.setWindow(self._windowManager.downloadView, **self._kwargs)
        if cid == self._bPrevious.getId():
            if self._kwargs['page'] > 1:
                self._kwargs['page'] = self._kwargs['page'] - 1
            self._windowManager.setWindow(self._windowManager.downloadView, **self._kwargs)

    def onAction(self, action):

        try:
            self.__focusColumn
        except AttributeError:
            self.__focusColumn = 0
            self.__focusRow = 0
            self.__focuspRow = 0
            self.__focusPagination = False


        if action.getId() == xbmcgui.ACTION_MOVE_RIGHT:
            if self.__focusColumn < (len(self.__buttonF) - 1) and self.__focusPagination == False:
                self.__focusColumn = self.__focusColumn + 1
                self.__focusRow = 0
            else:
                if self.__focuspRow < (len(self._paginationFocusList)-1):
                    self.__focuspRow = self.__focuspRow + 1


        if action.getId() == xbmcgui.ACTION_MOVE_LEFT:
            if self.__focusColumn > 0 and self.__focusPagination == False:
                self.__focusColumn = self.__focusColumn - 1
                self.__focusRow = 0
            else:
                if self.__focuspRow > 0:
                    self.__focuspRow = self.__focuspRow -1

        if action.getId() == xbmcgui.ACTION_MOVE_UP:
            if self.__focusRow > 0 and self.__focusPagination == False:
                self.__focusRow = self.__focusRow - 1
            else:
                self.__focusPagination = False



        if action.getId() == xbmcgui.ACTION_MOVE_DOWN:
            if self.__focusRow < (len(self.__buttonF[self.__focusColumn]) - 1):
                self.__focusRow = self.__focusRow + 1
            else:
                self.__focusPagination = True


        if self.__focusPagination == False:
                self.setFocusId(self.__buttonF[self.__focusColumn][self.__focusRow]['keyID'])
        else:
            self.setFocusId(self._paginationFocusList[self.__focuspRow])



class recordingView(OTRView):
    def __init__(self):

        OTRView.__init__(self, True)


        if 'recList' not in self._kwargs:
            self._kwargs['recList'] = self._api.getRecordings()


        self.__rowCount = 100

        self.__currentpageRowCount = 1
        self.__maxpageRowCount = 8
        self.__max = (self.__maxpageRowCount * self._kwargs['page'])
        self.__min = (self.__maxpageRowCount * (self._kwargs['page'] - 1))


        '''
        Column Title
        '''
        self.__focusList = None
        self.__detailButton = []
        self.__downloadButton = []
        self.__deleteButton = []
        # Title
        self.addControl(xbmcgui.ControlLabel(x=self._marginLeft + 50, y=self._marginTop + 50, width=500, height=25,
                                             label=u'Titel: '))



        for item in self._kwargs['recList']:
            if self.__currentpageRowCount <= self.__max and self.__currentpageRowCount > self.__min :
                self.addControl(
                    xbmcgui.ControlLabel(x=self._marginLeft + 50, y=self._marginTop + self.__rowCount, width=500, height=25,
                                         label=item["TITLE"]))

                # DetailButton
                button = xbmcgui.ControlButton(x=self._marginLeft + 770, y=self._marginTop + self.__rowCount, width=150,
                                               height=50, label="Details")
                self.addControl(button)
                self.__detailButton.append({button.getId(): item["EPGID"], 'keyID': button.getId()})

                # DownloadButton
                button = xbmcgui.ControlButton(x=self._marginLeft + 930, y=self._marginTop + self.__rowCount, width=150,
                                               height=50, label="Download")
                self.addControl(button)
                self.__downloadButton.append({button.getId(): item["EPGID"], 'keyID': button.getId()})


                # DeleteButton
                button = xbmcgui.ControlButton(x=self._marginLeft + 1090, y=self._marginTop + self.__rowCount, width=150,
                                               height=50, label="Löschen")
                self.addControl(button)
                self.__deleteButton.append({button.getId(): item["EPGID"], 'keyID': button.getId()})

                self.__rowCount = self.__rowCount + 55

            self.__currentpageRowCount = self.__currentpageRowCount + 1

        self.__focusList = [self.__detailButton, self.__downloadButton, self.__deleteButton]

    def onControl(self, control):

        OTRView.onControl(self, control)

        # detail
        for button in self.__detailButton:
            if control.getId() == button['keyID']:
                self._windowManager.setWindow(windowManager.programView, **{'epgID': button[control.getId()]})

        # download
        for button in self.__downloadButton:
            if control.getId() == button['keyID']:
                self._windowManager.setWindow(windowManager.downloadView, **{'epgID': button[control.getId()]})

        # delete
        for button in self.__deleteButton:
            if control.getId() == button['keyID']:

                result = xbmcgui.Dialog().yesno(heading="Aufnahme löschen",
                                                line1="Möchten Sie diese Aufnahme wirklich löschen.", yeslabel="JA",
                                                nolabel="NEIN")
                if result == 1:
                    self._api.deleteRecord(button[control.getId()])

        if control.getId() == self._bNext.getId():
            if self.__currentpageRowCount >= self.__max:
                self._kwargs['page'] = self._kwargs['page'] + 1
                self._windowManager.setWindow(self._windowManager.recordingView, **self._kwargs)
        if control.getId() == self._bPrevious.getId():
            if self._kwargs['page'] > 1:
                self._kwargs['page'] = self._kwargs['page'] - 1
            self._windowManager.setWindow(self._windowManager.recordingView, **self._kwargs)

    def onAction(self, action):

        try:
            self.__focusColumn
        except AttributeError:
            self.__focusColumn = 0
            self.__focusRow = 0
            self.__focuspRow = 0
            self.__focusPagination = False


        if action.getId() == xbmcgui.ACTION_MOVE_RIGHT:
            if self.__focusColumn < (len(self.__focusList) - 1) and self.__focusPagination == False:
                self.__focusColumn = self.__focusColumn + 1
            else:
                if self.__focuspRow < (len(self._paginationFocusList)-1):
                    self.__focuspRow = self.__focuspRow + 1


        if action.getId() == xbmcgui.ACTION_MOVE_LEFT:
            if self.__focusColumn > 0 and self.__focusPagination == False:
                self.__focusColumn = self.__focusColumn - 1
            else:
                if self.__focuspRow > 0:
                    self.__focuspRow = self.__focuspRow -1

        if action.getId() == xbmcgui.ACTION_MOVE_UP:
            if self.__focusRow > 0 and self.__focusPagination == False:
                self.__focusRow = self.__focusRow - 1
            else:
                self.__focusPagination = False



        if action.getId() == xbmcgui.ACTION_MOVE_DOWN:
            if self.__focusRow < (len(self.__focusList[self.__focusColumn]) - 1):
                self.__focusRow = self.__focusRow + 1
            else:
                self.__focusPagination = True


        if self.__focusPagination == False:
            if self.__focusList is not None:
                self.setFocusId(self.__focusList[self.__focusColumn][self.__focusRow]['keyID'])
        else:
            self.setFocusId(self._paginationFocusList[self.__focuspRow])

class searchView(OTRView):

    def __init__(self):


        OTRView.__init__(self, True)



        epgData = None
        if "epgData" in self._kwargs:
            epgData = self._kwargs["epgData"]
        else:


            keyboard = xbmc.Keyboard()
            keyboard.setHiddenInput(False)
            keyboard.setHeading('Suche')
            keyboard.doModal()
            if keyboard.isConfirmed():
                   timex = keyboard.getText()

                   if len(timex) == 0:
                       self._isClosed = True
                       self.close()

                   epgData = self._api.search(timex)
                   self._kwargs["epgData"] = epgData


        self.__rowCount = 100
        self.__currentpageRowCount = 1
        self.__maxpageRowCount = 8
        self.__max = (self.__maxpageRowCount * self._kwargs['page'])
        self.__min = (self.__maxpageRowCount * (self._kwargs['page'] - 1))

        self.__detailButton = []
        self.__downloadButton = []




        if epgData is not None:

            # Title
            self.addControl(xbmcgui.ControlLabel(x=self._marginLeft + 50, y=self._marginTop + 50, width=500, height=25,
                                             label=u'Titel: '))

            for item in epgData:
                if self.__currentpageRowCount <= self.__max and self.__currentpageRowCount > self.__min :
                    self.addControl(
                        xbmcgui.ControlLabel(x=self._marginLeft + 50, y=self._marginTop + self.__rowCount, width=500, height=25,
                                             label=item["TITLE"]))

                    # DetailButton
                    button = xbmcgui.ControlButton(x=self._marginLeft + 770, y=self._marginTop + self.__rowCount, width=150,
                                                   height=50, label="Details")
                    self.addControl(button)
                    self.__detailButton.append({button.getId(): item["EPGID"], 'keyID': button.getId()})

                    # DownloadButton
                    button = xbmcgui.ControlButton(x=self._marginLeft + 930, y=self._marginTop + self.__rowCount, width=150,
                                                   height=50, label="Download")
                    self.addControl(button)
                    self.__downloadButton.append({button.getId(): item["EPGID"], 'keyID': button.getId()})



                    self.__rowCount = self.__rowCount + 55

                self.__currentpageRowCount = self.__currentpageRowCount + 1


                self.__focusList = [self.__detailButton, self.__downloadButton]


    def onControl(self, control):

        OTRView.onControl(self, control)

        # detail
        for button in self.__detailButton:
            if control.getId() == button['keyID']:
                self._windowManager.setWindow(windowManager.programView, **{'epgID': button[control.getId()]})

        # download
        for button in self.__detailButton:
            if control.getId() == button['keyID']:
                self._windowManager.setWindow(windowManager.downloadView,
                                              **{'epgID': button[control.getId()]})

        if control.getId() == self._bNext.getId():
            if self.__currentpageRowCount >= self.__max:
                self._kwargs['page'] = self._kwargs['page'] + 1
                self._windowManager.setWindow(self._windowManager.searchView, **self._kwargs)
        if control.getId() == self._bPrevious.getId():
            if self._kwargs['page'] > 1:
                self._kwargs['page'] = self._kwargs['page'] - 1
            self._windowManager.setWindow(self._windowManager.searchView, **self._kwargs)

    def onAction(self, action):

        try:
            self.__focusColumn
        except AttributeError:
            self.__focusColumn = 0
            self.__focusRow = 0
            self.__focuspRow = 0
            self.__focusPagination = False


        if action.getId() == xbmcgui.ACTION_MOVE_RIGHT:
            if self.__focusColumn < (len(self.__focusList) - 1) and self.__focusPagination == False:
                self.__focusColumn = self.__focusColumn + 1
            else:
                if self.__focuspRow < (len(self._paginationFocusList)-1):
                    self.__focuspRow = self.__focuspRow + 1


        if action.getId() == xbmcgui.ACTION_MOVE_LEFT:
            if self.__focusColumn > 0 and self.__focusPagination == False:
                self.__focusColumn = self.__focusColumn - 1
            else:
                if self.__focuspRow > 0:
                    self.__focuspRow = self.__focuspRow -1

        if action.getId() == xbmcgui.ACTION_MOVE_UP:
            if self.__focusRow > 0 and self.__focusPagination == False:
                self.__focusRow = self.__focusRow - 1
            else:
                self.__focusPagination = False



        if action.getId() == xbmcgui.ACTION_MOVE_DOWN:
            if self.__focusRow < (len(self.__focusList[self.__focusColumn]) - 1):
                self.__focusRow = self.__focusRow + 1
            else:
                self.__focusPagination = True


        if self.__focusPagination == False:
            self.setFocusId(self.__focusList[self.__focusColumn][self.__focusRow]['keyID'])
        else:
            self.setFocusId(self._paginationFocusList[self.__focuspRow])

class ftppushView(OTRView):

    def __init__(self):


        OTRView.__init__(self, True)



        ftpData = self._api.getFTPPushJob()



        self.__rowCount = 100
        self.__currentpageRowCount = 1
        self.__maxpageRowCount = 8
        self.__max = (self.__maxpageRowCount * self._kwargs['page'])
        self.__min = (self.__maxpageRowCount * (self._kwargs['page'] - 1))

        self.__detailButton = []
        self.__downloadButton = []




        if ftpData is not None:

            # Title
            self.addControl(xbmcgui.ControlLabel(x=self._marginLeft + 50, y=self._marginTop + 50, width=500, height=25,
                                             label=u'Titel: '))

            for item in ftpData:
                if self.__currentpageRowCount <= self.__max and self.__currentpageRowCount > self.__min :
                    self.addControl(
                        xbmcgui.ControlLabel(x=self._marginLeft + 50, y=self._marginTop + self.__rowCount, width=500, height=25,
                                             label=item["FILENAME"]))

                    # DetailButton
                    button = xbmcgui.ControlButton(x=self._marginLeft + 770, y=self._marginTop + self.__rowCount, width=150,
                                                   height=50, label="Details")
                    self.addControl(button)
                    self.__detailButton.append({button.getId(): item["EPGID"], 'keyID': button.getId()})

                    # DownloadButton
                    button = xbmcgui.ControlButton(x=self._marginLeft + 930, y=self._marginTop + self.__rowCount, width=150,
                                                   height=50, label="Download")
                    self.addControl(button)
                    self.__downloadButton.append({button.getId(): item["EPGID"], 'keyID': button.getId()})



                    self.__rowCount = self.__rowCount + 55

                self.__currentpageRowCount = self.__currentpageRowCount + 1


                self.__focusList = [self.__detailButton, self.__downloadButton]


    def onControl(self, control):

        OTRView.onControl(self, control)

        # detail
        for button in self.__detailButton:
            if control.getId() == button['keyID']:
                self._windowManager.setWindow(windowManager.programView, **{'epgID': button[control.getId()]})

        # download
        for button in self.__detailButton:
            if control.getId() == button['keyID']:
                self._windowManager.setWindow(windowManager.downloadView,
                                              **{'epgID': button[control.getId()]})

        if control.getId() == self._bNext.getId():
            if self.__currentpageRowCount >= self.__max:
                self._kwargs['page'] = self._kwargs['page'] + 1
                self._windowManager.setWindow(self._windowManager.searchView, **self._kwargs)
        if control.getId() == self._bPrevious.getId():
            if self._kwargs['page'] > 1:
                self._kwargs['page'] = self._kwargs['page'] - 1
            self._windowManager.setWindow(self._windowManager.searchView, **self._kwargs)

    def onAction(self, action):

        try:
            self.__focusColumn
        except AttributeError:
            self.__focusColumn = 0
            self.__focusRow = 0
            self.__focuspRow = 0
            self.__focusPagination = False


        if action.getId() == xbmcgui.ACTION_MOVE_RIGHT:
            if self.__focusColumn < (len(self.__focusList) - 1) and self.__focusPagination == False:
                self.__focusColumn = self.__focusColumn + 1
            else:
                if self.__focuspRow < (len(self._paginationFocusList)-1):
                    self.__focuspRow = self.__focuspRow + 1


        if action.getId() == xbmcgui.ACTION_MOVE_LEFT:
            if self.__focusColumn > 0 and self.__focusPagination == False:
                self.__focusColumn = self.__focusColumn - 1
            else:
                if self.__focuspRow > 0:
                    self.__focuspRow = self.__focuspRow -1

        if action.getId() == xbmcgui.ACTION_MOVE_UP:
            if self.__focusRow > 0 and self.__focusPagination == False:
                self.__focusRow = self.__focusRow - 1
            else:
                self.__focusPagination = False



        if action.getId() == xbmcgui.ACTION_MOVE_DOWN:
            if self.__focusRow < (len(self.__focusList[self.__focusColumn]) - 1):
                self.__focusRow = self.__focusRow + 1
            else:
                self.__focusPagination = True


        if self.__focusPagination == False:
            self.setFocusId(self.__focusList[self.__focusColumn][self.__focusRow]['keyID'])
        else:
            self.setFocusId(self._paginationFocusList[self.__focuspRow])

class mediaView(OTRView):

    def __init__(self):


        OTRView.__init__(self, True)

        mediaList = None

        if 'media' in self._kwargs and 'refresh' in self._kwargs:
            if self._kwargs['refresh'] == True:
                mediaList = filesystem.filesystem().findALLVideos()
                mediaList.extend(filesystem.filesystem().findOTRKeyFiles())
                self._kwargs['media'] = mediaList
                self._kwargs['refresh'] = False
            else:
                mediaList = self._kwargs['media']
        else:
            mediaList = filesystem.filesystem().findALLVideos()
            mediaList.extend(filesystem.filesystem().findOTRKeyFiles())
            self._kwargs['media'] = mediaList
            self._kwargs['refresh'] = False



        self.__rowCount = 100
        self.__currentpageRowCount = 1
        self.__maxpageRowCount = 8
        self.__max = (self.__maxpageRowCount * self._kwargs['page'])
        self.__min = (self.__maxpageRowCount * (self._kwargs['page'] - 1))

        self.__Button = []
        self.__focusList = None





        if mediaList is not None:

            # Title
            self.addControl(xbmcgui.ControlLabel(x=self._marginLeft + 50, y=self._marginTop + 50, width=500, height=25,
                                             label=u'Titel: '))

            for item in mediaList:
                if self.__currentpageRowCount <= self.__max and self.__currentpageRowCount > self.__min :

                    name = item.split(os.sep)
                    name = name[len(name)-1]

                    self.addControl(
                        xbmcgui.ControlLabel(x=self._marginLeft + 50, y=self._marginTop + self.__rowCount, width=800, height=25,
                                             label=name))

                    # viewButton
                    button = xbmcgui.ControlButton(x=self._marginLeft + 930, y=self._marginTop + self.__rowCount, width=150,
                                                   height=50, label="Ansehen")
                    self.addControl(button)
                    self.__Button.append({button.getId(): item, 'keyID': button.getId()})



                    self.__rowCount = self.__rowCount + 55

                self.__currentpageRowCount = self.__currentpageRowCount + 1


                self.__focusList = self.__Button


    def onControl(self, control):

        OTRView.onControl(self, control)


        # view
        for button in self.__Button:

            if control.getId() == button['keyID']:
                if '.otr' in button[control.getId()]:
                    result = xbmcgui.Dialog().yesno(heading="Aufnahmeliste",
                                                line1="Die Aufnahme ist nocht nicht decodiert. Soll sie jetzt decodiert werden?", yeslabel="JA",
                                                nolabel="NEIN")

                    if result == 1:
                        de = decoder.easydecoder()
                        de.decode(button[control.getId()])
                else:
                    xbmc.Player().play(button[control.getId()])



        if control.getId() == self._bNext.getId():
            if self.__currentpageRowCount >= self.__max:
                self._kwargs['page'] = self._kwargs['page'] + 1
                self._windowManager.setWindow(self._windowManager.mediaView, **self._kwargs)
        if control.getId() == self._bPrevious.getId():
            if self._kwargs['page'] > 1:
                self._kwargs['page'] = self._kwargs['page'] - 1
            self._windowManager.setWindow(self._windowManager.mediaView, **self._kwargs)

    def onAction(self, action):

        try:
            self.__focusColumn
        except AttributeError:
            self.__focusColumn = 0
            self.__focusRow = 0
            self.__focuspRow = 0
            self.__focusPagination = False


        if action.getId() == xbmcgui.ACTION_MOVE_RIGHT:
            if self.__focuspRow < (len(self._paginationFocusList)-1):
                self.__focuspRow = self.__focuspRow + 1


        if action.getId() == xbmcgui.ACTION_MOVE_LEFT:
            if self.__focuspRow > 0:
                self.__focuspRow = self.__focuspRow -1

        if action.getId() == xbmcgui.ACTION_MOVE_UP:
            if self.__focusRow > 0 and self.__focusPagination == False:
                self.__focusRow = self.__focusRow - 1
            else:
                self.__focusPagination = False



        if action.getId() == xbmcgui.ACTION_MOVE_DOWN:
            if self.__focusRow < (len(self.__focusList) - 1):
                self.__focusRow = self.__focusRow + 1
            else:
                self.__focusPagination = True


        if self.__focusPagination == False:
            if self.__focusList is not None:
                self.setFocusId(self.__focusList[self.__focusRow]['keyID'])
        else:
            self.setFocusId(self._paginationFocusList[self.__focuspRow])


class infoView(OTRView):

    def __init__(self):


        OTRView.__init__(self, True)

        self.__rowCount = 100
        self.__currentpageRowCount = 1
        self.__maxpageRowCount = 1
        self.__max = (self.__maxpageRowCount * self._kwargs['page'])
        self.__min = (self.__maxpageRowCount * (self._kwargs['page'] - 1))

        self.__Button = []

        self._addon = xbmcaddon.Addon()
        __cwd__ = xbmc.translatePath(self._addon.getAddonInfo('path')).decode("utf-8")
        self.__disc = os.path.join(__cwd__, 'resources', 'data', 'disclaimer.txt')

        #lines = open(self.__disc, "r").read().splitlines()

        with open(self.__disc, 'r') as myfile:
            lines=myfile.read().replace('\n', '')

        disclaimer = lines.split('[NewSite]')


        #imgp = r"\[Img\](.*?)\[\/Img\]"
        imgp = r"\[Img ([^\]]*)\]([^\[]*)\[/[^\]]*\]"
        imgs = re.findall(imgp, disclaimer[self._kwargs['page']-1])

        #print imgs


        disclaimersub = re.split(imgp, disclaimer[self._kwargs['page']-1])

        #remove empty el
        disclaimersub = [var for var in disclaimersub if var]



        imgL = []
        #remove url
        for img in imgs:
            tmp = re.findall(r'height=([\'"])(.*?)([\'"])', img[0])
            tmp1 = re.findall(r'width=([\'"])(.*?)([\'"])', img[0])
            url = img[1]

            imgL.append({'height':int(tmp[0][1]), 'width':int(tmp1[0][1]),'url':url})


            disclaimersub.remove(url)
            disclaimersub.remove(img[0])




        for line in disclaimersub:



            self.addControl(xbmcgui.ControlLabel(x=50, y=self.__rowCount, width=700, height=350,
                                             label=textwrap.fill(line, 70)))

            if self.__rowCount <= 100:
                self.__rowCount += 260
            else:
                self.__rowCount += 360

            for img in imgL:
                #print img
                self.addControl(xbmcgui.ControlImage(50, self.__rowCount, img['width'], img['height'], img['url']))

                self.__rowCount += img['height'] + 10


        self.__currentpageRowCount = (len(disclaimer) - 1)


        print self._kwargs['page']
        print len(disclaimer)

        if self._kwargs['page'] >= len(disclaimer):
            # DownloadButton
            button = xbmcgui.ControlButton(x=600, y=630, width=300,
                                           height=50, label="Ich bin damit Einverstanden!")
            self.addControl(button)
            self.__Button.append({'keyID': button.getId(), 'service': 'accept'})



        self.__focusList = [self.__Button]


    def onControl(self, control):

        OTRView.onControl(self, control)

        # Buttons
        for button in self.__Button:
            if control.getId() == button['keyID'] and button['service'] == 'accept':
                print "Terms accepted----------------------"
                __cfg = config.Config()
                __cfg.set(DisclaimerAccepted=True)
                self.setClosed(True)

                self._addon.openSettings()



        if control.getId() == self._bNext.getId():
            if self.__currentpageRowCount >= self.__max:
                self._kwargs['page'] = self._kwargs['page'] + 1
                self._windowManager.setWindow(self._windowManager.infoView, **self._kwargs)
        if control.getId() == self._bPrevious.getId():
            if self._kwargs['page'] > 1:
                self._kwargs['page'] = self._kwargs['page'] - 1
            self._windowManager.setWindow(self._windowManager.infoView, **self._kwargs)

    def onAction(self, action):

        try:
            self.__focusColumn
        except AttributeError:
            self.__focusColumn = 0
            self.__focusRow = 0
            self.__focuspRow = 0
            self.__focusPagination = False


        if action.getId() == xbmcgui.ACTION_MOVE_RIGHT:
            if self.__focusColumn < (len(self.__focusList) - 1) and self.__focusPagination == False:
                self.__focusColumn = self.__focusColumn + 1
            else:
                if self.__focuspRow < (len(self._paginationFocusList)-1):
                    self.__focuspRow = self.__focuspRow + 1


        if action.getId() == xbmcgui.ACTION_MOVE_LEFT:
            if self.__focusColumn > 0 and self.__focusPagination == False:
                self.__focusColumn = self.__focusColumn - 1
            else:
                if self.__focuspRow > 0:
                    self.__focuspRow = self.__focuspRow -1

        if action.getId() == xbmcgui.ACTION_MOVE_UP:
            if self.__focusRow > 0 and self.__focusPagination == False:
                self.__focusRow = self.__focusRow - 1
            else:
                self.__focusPagination = False



        if action.getId() == xbmcgui.ACTION_MOVE_DOWN:
            if self.__focusRow < (len(self.__focusList[self.__focusColumn]) - 1):
                self.__focusRow = self.__focusRow + 1
            else:
                self.__focusPagination = True


        if self.__focusPagination == False:
            self.setFocusId(self.__focusList[self.__focusColumn][self.__focusRow]['keyID'])
        else:
            self.setFocusId(self._paginationFocusList[self.__focuspRow])