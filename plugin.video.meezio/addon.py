#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import sys
from datetime import datetime, timedelta

import xbmc
import xbmcaddon
from xbmcswift2 import Plugin

Addon = xbmcaddon.Addon()
__cwd__ = xbmc.translatePath(Addon.getAddonInfo('path')).decode("utf-8")
BASE_RESOURCE_PATH = os.path.join(__cwd__, 'resources', 'lib')

sys.path.append(BASE_RESOURCE_PATH)

import resources.lib.slogo as slogo
import resources.lib.api as api

import resources.lib.gui as gui
import resources.lib.config as config

import mirror
import filesystem
import decoder



try:
    import StorageServer
except:
    import storageserverdummy as StorageServer

cache = StorageServer.StorageServer("myOTR/addon", 24)


plugin = Plugin()


@plugin.route('/')
def index():

    '''
    {
        'label': 'FTP-Push'.decode("utf-8"),
        'icon': logo.getByProgName('CHANNELSEP'),
        'endpoint': 'ftppushView',
        'endpointParam': {}
    },
    '''



    rootItems = [

        {
            'label': 'Aufnahmen'.decode("utf-8"),
            'icon': logo.getByProgName('CHANNELSEP'),
            'endpoint': 'recordingsView',
            'endpointParam': {}
        },

        {
            'label': 'Downloads'.decode("utf-8"),
            'icon': logo.getByProgName('CHANNELSEP'),
            'endpoint': 'downloadView',
            'endpointParam': {}
        },

        {
            'label': 'Fernsehen'.decode("utf-8"),
            'icon': logo.getByProgName('CHANNELSEP'),
            'endpoint': 'languageView',
            'endpointParam': {}
        },

        {
            'label': 'Settings'.decode("utf-8"),
            'icon': logo.getByProgName('CHANNELSEP'),
            'endpoint': 'settingsView',
            'endpointParam': {}
        },

        {
            'label': 'Stöbern'.decode("utf-8"),
            'icon': logo.getByProgName('CHANNELSEP'),
            'endpoint': 'highlightOverView',
            'endpointParam': {}
        },

        {
            'label': 'Suchen'.decode("utf-8"),
            'icon': logo.getByProgName('CHANNELSEP'),
            'endpoint': 'searchView',
            'endpointParam': {}
        },

    ]
    addItem(rootItems)


@plugin.route('/highlightOverView/')
def highlightOverView():
    rootItems = [

        {
            'label': 'Top Sendungen, Zukunft'.decode("utf-8"),
            'icon': logo.getByProgName('CHANNELSEP'),
            'endpoint': 'highlightView',
            'endpointParam': {'rss': 'highlights_future'}
        },

        {
            'label': 'Top Aufnahmen, Vergangenheit'.decode("utf-8"),
            'icon': logo.getByProgName('CHANNELSEP'),
            'endpoint': 'highlightView',
            'endpointParam': {'rss': 'highlights_past'}
        },

        {
            'label': 'Top Aufnahmen, Vergangenheit, in HQ'.decode("utf-8"),
            'icon': logo.getByProgName('CHANNELSEP'),
            'endpoint': 'highlightView',
            'endpointParam': {'rss': 'highlights_past_hq'}
        },

        {
            'label': 'Top Aufnahmen, Vergangenheit, in HD'.decode("utf-8"),
            'icon': logo.getByProgName('CHANNELSEP'),
            'endpoint': 'highlightView',
            'endpointParam': {'rss': 'highlights_past_hd'}
        },



    ]
    addItem(rootItems)

@plugin.route('/highlightView/<rss>/')
def highlightView(rss):
    highl = api.getRSS(get=rss)
    print rss
    print highl

    guir = gui.windowManager()
    guir.setWindow(gui.windowManager.searchView, epgData=highl)
    guir.show()

@plugin.route('/searchView/')
def searchView():
    guir = gui.windowManager()
    guir.setWindow(gui.windowManager.searchView)
    guir.show()

@plugin.route('/ftppushView/')
def ftppushView():
    guir = gui.windowManager()
    guir.setWindow(gui.windowManager.ftppushView)
    guir.show()

@plugin.route('/downloadView/')
def downloadView():
    guir = gui.windowManager()
    guir.setWindow(gui.windowManager.mediaView)
    guir.show()


@plugin.route('/recordingsView/')
def recordingsView():
    guir = gui.windowManager()
    guir.setWindow(gui.windowManager.recordingView)
    guir.show()


@plugin.route('/settingsView/')
def settingsView():
    addon.openSettings()


@plugin.route('/languageView/')
def languageView():
    rootItems = [

        {
            'label': 'Favoriten'.decode("utf-8"),
            'icon': logo.getByProgName('FAVORITEN'),
            'endpoint': 'favoriten',
            'endpointParam': {'lang': ''}
        },

        {
            'label': 'Alle'.decode("utf-8"),
            'icon': logo.getByProgName('CHANNELSEP'),
            'endpoint': 'stationView',
            'endpointParam': {'lang': 'all'}
        },

        {
            'label': 'Deutschland'.decode("utf-8"),
            'icon': logo.getByProgName('CHANNELSEP'),
            'endpoint': 'stationView',
            'endpointParam': {'lang': 'DE'}
        },

        {
            'label': 'Östereich'.decode("utf-8"),
            'icon': logo.getByProgName('CHANNELSEP'),
            'endpoint': 'stationView',
            'endpointParam': {'lang': 'AT'}
        },

        {
            'label': 'Spanisch'.decode("utf-8"),
            'icon': logo.getByProgName('CHANNELSEP'),
            'endpoint': 'stationView',
            'endpointParam': {'lang': 'ES'}
        },

        {
            'label': 'Französisch'.decode("utf-8"),
            'icon': logo.getByProgName('CHANNELSEP'),
            'endpoint': 'stationView',
            'endpointParam': {'lang': 'FR'}
        },

        {
            'label': 'Italienisch'.decode("utf-8"),
            'icon': logo.getByProgName('CHANNELSEP'),
            'endpoint': 'stationView',
            'endpointParam': {'lang': 'IT'}
        },

        {
            'label': 'Polnisch'.decode("utf-8"),
            'icon': logo.getByProgName('CHANNELSEP'),
            'endpoint': 'stationView',
            'endpointParam': {'lang': 'PL'}
        },

        {
            'label': 'Russisch'.decode("utf-8"),
            'icon': logo.getByProgName('CHANNELSEP'),
            'endpoint': 'stationView',
            'endpointParam': {'lang': 'RU'}
        },

        {
            'label': 'Türkisch'.decode("utf-8"),
            'icon': logo.getByProgName('CHANNELSEP'),
            'endpoint': 'stationView',
            'endpointParam': {'lang': 'TR'}
        },

        {
            'label': 'Britisch'.decode("utf-8"),
            'icon': logo.getByProgName('CHANNELSEP'),
            'endpoint': 'stationView',
            'endpointParam': {'lang': 'UK'}
        },

        {
            'label': 'Amerikanisch'.decode("utf-8"),
            'icon': logo.getByProgName('CHANNELSEP'),
            'endpoint': 'stationView',
            'endpointParam': {'lang': 'US'}
        },

    ]
    addItem(rootItems)


@plugin.route('/stationView/<lang>/')
def stationView(lang):
    plist = cache.cacheFunction(api.getProgramList)

    for el in list(plist):
        el['endpoint'] = 'dateView'
        el['endpointParam'] = {'station': el['TITLE']}
        el['label'] = el['TITLE']
        del el['TITLE']

        if lang not in (el['COUNTRY'], el['LANGUAGE']) and 'all' not in lang:
            plist.remove(el)

    addItem(plist)


@plugin.route('/dateView/<station>/')
def dateView(station):
    today = datetime.now().strftime("%d.%m.%Y")

    rootItems = []

    for i in range(7, 0, -1):
        date_N_days_ago = datetime.now() - timedelta(days=i)
        date_N_days_ago = date_N_days_ago.strftime("%d.%m.%Y")

        rootItems.append(
            {
                'label': date_N_days_ago.decode("utf-8"),
                'icon': logo.getByProgName('CHANNELSEP'),
                'endpoint': 'stationDetails',
                'endpointParam': {'station': station, 'date': date_N_days_ago.decode("utf-8")}

            }
        )

    rootItems.append(
        {
            'label': u'[COLOR green] %s [/COLOR]' % (today.decode("utf-8")),
            'icon': logo.getByProgName('CHANNELSEP'),
            'endpoint': 'stationDetails',
            'endpointParam': {'station': station, 'date': today.decode("utf-8")}
        }
    )

    for i in range(1, 8):
        date_in_N_days = datetime.now() + timedelta(days=i)
        date_in_N_days = date_in_N_days.strftime("%d.%m.%Y")

        rootItems.append(
            {
                'label': date_in_N_days.decode("utf-8"),
                'icon': logo.getByProgName('CHANNELSEP'),
                'endpoint': 'stationDetails',
                'endpointParam': {'station': station, 'date': date_in_N_days.decode("utf-8")}

            }
        )

    addItem(rootItems, viewMode=504)


@plugin.route('/stationDetails/<station>/<date>/')
def stationDetails(station, date):
    epglist = cache.cacheFunction(api.getEPG, station, date, date)

    rootItems = []

    for el in epglist:
        rootItems.append(
            {
                'label': datetime.fromtimestamp(int(el["BEGINN"])).strftime('%H:%M').decode("utf-8") + " - " +
                         datetime.fromtimestamp(int(el["ENDE"])).strftime('%H:%M').decode("utf-8") + " | " + el[
                             "TITEL"].replace("+", " ").decode("utf-8"),
                'icon': logo.getByProgName('CHANNELSEP'),
                'endpoint': 'programView',
                'endpointParam': {'epgID': el["ID"]}

            }
        )

    addItem(rootItems, viewMode=504)


@plugin.route('/programView/<epgID>/')
def programView(epgID):
    guir = gui.windowManager()
    guir.setWindow(gui.windowManager.programView, epgID=epgID)
    #guir.windowManager()
    guir.show()


@plugin.route('/favoriten/')
def favoriten():
    print api.getProgramList()


def addItem(itemList, viewMode=None):
    count = 1
    items = []
    for item in itemList:

        routeParam = item.get('endpointParam')

        if item.get('icon') is None:
            icon = logo.getByProgName(item.get('label'))
        else:
            icon = item.get('icon')

        title = u'[COLOR red][ %s ][/COLOR] %s' % (count, item.get('label'))

        additem = {
            'label': title,
            'icon': icon,
            'info': {
                'count': 1,
                'studio': "ich",
                'genre': 'genre',
                'tagline': 'tagline',
                'plot': 'plot',
                'date': 'date'
            },
            'is_playable': False
        }

        if item.get('endpointParam') is None:
            additem.update({'path': plugin.url_for(item.get('endpoint'), **{'lang': 'all'})})
        else:
            additem.update({'path': plugin.url_for(endpoint=item.get('endpoint'), **routeParam)})

        items.append(additem)

        count = count + 1

    finish_kwargs = {
        'sort_methods': ('PLAYLIST_ORDER', 'DATE'),
    }

    if viewMode is None:
        finish_kwargs.update({'view_mode': 'thumbnail'})
    else:
        finish_kwargs.update({'view_mode': viewMode})

    return plugin.finish(items, **finish_kwargs)


if __name__ == '__main__':

    __cfg = config.Config()
    if __cfg.get('DisclaimerAccepted') == 'False':
        guir = gui.windowManager()
        guir.setWindow(gui.windowManager.infoView)
        guir.show()

    addon = xbmcaddon.Addon()
    logo = slogo.Slogo()
    api = api.Api()
    plugin.run()

