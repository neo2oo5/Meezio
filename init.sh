#!/usr/bin/env bash


ROOT="$HOME/.kodi/addons/"
ADEVROOT="Meezio/"
AROOT="plugin.video.meezio/"

if [ ! -d "$ROOT$AROOT" ]; then

  ln -s "$ROOT$ADEVROOT$AROOT" "$ROOT"

fi


