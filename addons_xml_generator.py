# *
# *  Copyright (C) 2012-2013 Garrett Brown
# *  Copyright (C) 2010      j48antialias
# *
# *  This Program is free software; you can redistribute it and/or modify
# *  it under the terms of the GNU General Public License as published by
# *  the Free Software Foundation; either version 2, or (at your option)
# *  any later version.
# *
# *  This Program is distributed in the hope that it will be useful,
# *  but WITHOUT ANY WARRANTY; without even the implied warranty of
# *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# *  GNU General Public License for more details.
# *
# *  You should have received a copy of the GNU General Public License
# *  along with XBMC; see the file COPYING.  If not, write to
# *  the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
# *  http://www.gnu.org/copyleft/gpl.html
# *
# *  Based on code by j48antialias:
# *  https://anarchintosh-projects.googlecode.com/files/addons_xml_generator.py

""" self.__addonxml generator """

import os
import sys
import zipfile
from shutil import copyfile
import xml.etree.ElementTree as ET
import inspect
import ConfigParser

# Compatibility with 3.0, 3.1 and 3.2 not supporting u"" literals
if sys.version < '3':
    import codecs
    def u(x):
        return codecs.unicode_escape_decode(x)[0]
else:
    def u(x):
        return x

class Generator:
    """
        Generates a new self.__addonxml file from each addons addon.xml file
        and a new self.__addonxml.md5 hash file. Must be run from the root of
        the checked-out repo. Only handles single depth folder structure.
    """
    def __init__( self ):

        self.__cfg = Config()

        self.__cfg.set(DisclaimerAccepted='False', DevMode='False')

        self.__update = "update/"
        self.__root = "plugin.video.meezio/"
        self.__addonxml = self.__update+"addons.xml"
        # addon list
        self.__addons = ['plugin.video.meezio', 'repository.meezio']
        # generate files
        self._generate_addons_file()
        self._generate_md5_file()

        self.__cfg.set(DisclaimerAccepted='True', DevMode='True')
        # notify user
        print("Finished updating addons xml and md5 files")

    def _generate_zip(self, src, dst):

        zf = zipfile.ZipFile("%s.zip" % (dst), "w", zipfile.ZIP_DEFLATED)
        abs_src = os.path.abspath(src)
        for dirname, subdirs, files in os.walk(src):
            for filename in files:
                absname = os.path.abspath(os.path.join(dirname, filename))
                arcname = absname[len(abs_src) + 1:]
                print 'zipping %s as %s' % (os.path.join(dirname, filename),
                                            arcname)
                zf.write(absname, os.path.join(src,arcname))
        zf.close()

    def _generate_addons_file( self ):

        # final addons text
        addons_xml = u("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n<addons>\n")
        # loop thru and add each addons addon.xml file
        for addon in self.__addons:
            try:
                # skip any file or .svn folder or .git folder

                if ( not os.path.isdir( addon ) or addon == ".svn" or addon == ".git" ): continue
                # create path
                _path = os.path.join(addon, "addon.xml" )
                # split lines for stripping
                xml_lines = open(_path, "r").read().splitlines()

                # new addon
                addon_xml = ""
                # loop thru cleaning each line
                for line in xml_lines:

                    # skip encoding format line
                    if (line.find( "<?xml" ) >= 0 ): continue
                    # add line
                    if sys.version < '3':

                        addon_xml += unicode(line.rstrip() + "\n", "UTF-8")

                    else:
                        addon_xml += line.rstrip() + "\n"
                # we succeeded so add to our final self.__addonxml text
                addons_xml += addon_xml.rstrip() + "\n\n"
                #create zip

                root = ET.fromstring(open(_path, "r").read())


                self._generate_zip(addon, os.path.join('update', addon, addon+"-"+root.attrib['version']))
                #copy changelog
                copyfile(os.path.join(addon, 'changelog.txt'), os.path.join('update', addon, 'changelog.txt'))
                

            except Exception as e:
                # missing or poorly formatted addon.xml
                print("Excluding %s for %s" % ( _path, e ))
        # clean and add closing tag
        addons_xml = addons_xml.strip() + u("\n</addons>\n")
        # save file
        self._save_file( addons_xml.encode( "UTF-8" ), file=self.__addonxml )

    def _generate_md5_file( self ):
        # create a new md5 hash
        try:
            import md5
            m = md5.new( open( self.__addonxml, "r" ).read() ).hexdigest()
        except ImportError:
            import hashlib
            m = hashlib.md5( open( self.__addonxml, "r", encoding="UTF-8" ).read().encode( "UTF-8" ) ).hexdigest()

        # save file
        try:
            self._save_file( m.encode( "UTF-8" ), file=self.__update+"addons.xml.md5" )
        except Exception as e:
            # oops
            print("An error occurred creating self.__addonxml.md5 file!\n%s" % e)

    def _save_file( self, data, file ):
        try:
            # write data to the file (use b for Python 3)
            open( file, "wb" ).write( data )
        except Exception as e:
            # oops
            print("An error occurred saving %s file!\n%s" % ( file, e ))

class Singleton(type):
    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]

class Config:
    __metaclass__ = Singleton

    def __init__(self):

        __cwd__ = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
        self.__CONFIG = os.path.join(__cwd__, 'plugin.video.meezio', 'resources', 'data', 'config.cfg')

        self.__defaultSection = "DEFAULT"
        self.__config = ConfigParser.ConfigParser()
        self.__config.read(self.__CONFIG)


    """
    cfg.set(**{'tk1': 'vl1', 'tk2': 'vl2'})
    cfg.set(lk1='vl1', lk2='vl2')
    """
    def set(self, **kwargs):

        for key, value in kwargs.iteritems():
            self.__config.set(self.__defaultSection, key, value)

        with open(self.__CONFIG, 'wb') as configfile:
            self.__config.write(configfile)

    def get(self, key):
        return self.__config.get(self.__defaultSection, key)

if ( __name__ == "__main__" ):
    # start
    Generator()