import hashlib
import time
#log pfad
logfile = "/home/xoxoxo/.kodi/temp/kodi.log"

def md5(fname):
    hash = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash.update(chunk)
    return hash.hexdigest()


lasthex = None


while True:
    currenthex = md5(logfile)
    if lasthex == currenthex:
        time.sleep(2)
    else:
        lasthex = currenthex
        with open(logfile) as f:
            file = f.readlines()
            for fi in file:
                fi = fi.strip()
                if fi:
                    print fi

        f.close()
